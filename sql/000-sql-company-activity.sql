SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

INSERT INTO `company_activity` (`id`, `name`) VALUES
(1, 'Agroalimentaire'),
(2, 'Bois, Papier, Carton, Imprimerie'),
(3, 'Chimie, Para chimie'),
(4, 'Edition, Communication, Multimédia'),
(5, 'Etudes et Conseils'),
(6, 'Informatique, Télécom'),
(7, 'Métallurgie, Travail du métal'),
(8, 'Service aux entreprises'),
(9, 'Transport, Logistique'),
(10, 'Banque, Assurance'),
(11, 'BTP, Matériaux de construction'),
(12, 'Commerce, Négoce, Distribution'),
(13, 'Electronique, Electricité'),
(14, 'Industrie pharmaceutique'),
(15, 'Machine et équipement, Automobile'),
(16, 'Plastique, Caoutchouc'),
(17, 'Textile, Habillement, Chaussures'),
(18, 'Profession libérale');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
