SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

INSERT INTO `interest` (`id`, `name`) VALUES
(1, 'Chant'),
(2, 'Cosplay'),
(3, 'Couture'),
(4, 'Cuisine'),
(5, 'Danse'),
(6, 'Décoration'),
(7, 'Ecriture'),
(8, 'Films et TV'),
(9, 'Informatique'),
(10, 'Jeux vidéo'),
(11, 'Lecture'),
(12, 'Musique'),
(13, 'Peinture'),
(14, 'Scrapbooking'),
(15, 'Sport'),
(16, 'Théâtre'),
(17, 'Voyage'),
(18, 'Photographie'),
(19, 'Spirituel');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
