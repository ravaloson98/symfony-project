SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

INSERT INTO `cursus` (`id`, `name`) VALUES
-- L1 & L2
(1, 'Marketing et Communication'),
(2, 'Commerce International'),
(3, 'Comptabilité et Finance'),
(4, 'Gestion et Ressources Humaines'),
(5, 'Management et Developpement D''Entreprises'),
-- M1
(6, 'Marketing Management et Innovation'),
(7, 'International Business'),
(8, 'Audit et Finance'),
(9, 'Management des Ressources Humaines'),
(10, 'Création et Développement d''Affaires'),
(11, 'Design et Innovation'),
-- DES, DESA & DEPA
(12, 'Ressources Humaines'),
-- DES
(13, 'Comptabilité'),
-- DESA
(14, 'Projet'),
-- DEPA
(15, 'Audit et Contrôle de gestion');



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
