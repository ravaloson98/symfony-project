<?php

namespace App\Form\Type;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\Applicant;

class StepThreeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
          $builder
              ->add('type', IntegerType::class)
              ->add('diploma_year', IntegerType::class)
              ->add('diploma_place', TextType::class)
          ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $applicant = $event->getData();
            $form = $event->getForm();

            if ($applicant->getType() == 2) {
                $form
                    ->add('sat_ebrw_grade', IntegerType::class)
                    ->add('sat_algebra_grade', IntegerType::class)
                ;
            } else {
                $form
                    ->add('baccalaureate_type', IntegerType::class)
                    ->add('baccalaureate_acknowledgement', IntegerType::class)
                    ->add('baccalaureate_grade', NumberType::class)
                ;
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Applicant::class,
            'csrf_protection' => false,
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();

                return ($data->getType() == 1) ? ['baccalaureate'] : ['sat'];
            }
        ]);
    }
}
