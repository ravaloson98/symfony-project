<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\Common\Collections\ArrayCollection;

use App\Entity\Person;
use App\Entity\JobType;
use App\Entity\JobTitle;
use App\Entity\CompanyActivity;

class StepTwoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, ['choices' => [ 20, 30, 40 ], 'required' => true ])
            ->add('last_name', TextType::class)
            ->add('first_name', TextType::class, ['required' => true])
            ->add('usage_name', TextType::class)
            ->add('primary_email', TextType::class) // enforce email TODO
            ->add('secondary_address', TextType::class)
            ->add('job_type', EntityType::class, ['class' => JobType::class, 'required' => true])
            ->add('job_title', EntityType::class, ['class' => JobTitle::class, 'required' => true])
            ->add('job_details', TextType::class)
            ->add('company', TextType::class)
            ->add('company_activity', EntityType::class, ['class' => CompanyActivity::class, 'required' => true])
            ->add('company_phone', TextType::class, ['required' => true])
            ->add('company_address', TextType::class, ['required' => true])
            ->add('phone', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Person::class,
            'csrf_protection' => false,
            'validation_groups' => ['parent']
        ]);
    }
}
