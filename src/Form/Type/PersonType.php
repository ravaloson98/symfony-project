<?php

namespace App\Form\Type;

use App\Entity\Country;
use App\Entity\Person;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('last_name', TextType::class, [ 'error_bubbling' => false ])
            ->add('first_name', TextType::class, ['required' => true, 'error_bubbling' => false])
            ->add('usage_name', TextType::class, ['required' => true])
            ->add('birthdate', DateType::class, ['required' => true, 'html5' => false, 'widget' => 'single_text', 'format' => 'dd/MM/yyyy'])
            ->add('sex', ChoiceType::class, ['choices' => [ 0, 1 ]])
            ->add('birth_city', TextType::class, ['required' => true])
            ->add('birth_country', EntityType::class, ['class' => Country::class, 'required' => true])
            ->add('nationality', EntityType::class, ['class' => Country::class, 'required' => true])
            ->add('marital_status', ChoiceType::class, ['choices' => [ 10, 20, 30, 40 ]])
            ->add('address', TextType::class, ['required' => true])
            ->add('secondary_address', TextType::class)
            ->add('mobile_phone', TextType::class, ['required' => true])
            ->add('phone', TextType::class)
            ->add('user', UserType::class, ['required' => true])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'validation_groups' => ['student']
        ]);
    }
}
