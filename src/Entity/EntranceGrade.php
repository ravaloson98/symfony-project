<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EntranceGradeRepository;

/**
 * @ORM\Entity(repositoryClass=EntranceGradeRepository::class)
 */
class EntranceGrade
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="SchoolYear", inversedBy="entranceGrades", cascade={"all"})
     * @ORM\JoinTable(name="school_years_entrance_grades")
     */
    private $schoolYears;

    /**
     * @ORM\ManyToMany(targetEntity="Cursus", mappedBy="entranceGrades", cascade={"all"})
     */
    private $cursuses;

    public function __construct()
    {
        $this->schoolYears = new ArrayCollection();
        $this->cursuses = new ArrayCollection();
    }

    public function toArray()
    {
        return [
            'id'   => $this->getId(),
            'name' => $this->getName()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|SchoolYear[]
     */
    public function getSchoolYears(): Collection
    {
        return $this->schoolYears;
    }

    public function addSchoolYear(SchoolYear $schoolYear): self
    {
        if (!$this->schoolYears->contains($schoolYear)) {
            $this->schoolYears[] = $schoolYear;
        }

        return $this;
    }

    public function removeSchoolYear(SchoolYear $schoolYear): self
    {
        if ($this->schoolYears->contains($schoolYear)) {
            $this->schoolYears->removeElement($schoolYear);
        }

        return $this;
    }

    /**
     * @return Collection|Cursus[]
     */
    public function getCursuses(): Collection
    {
        return $this->cursuses;
    }

    public function addCursus(Cursus $cursus): self
    {
        if (!$this->cursuses->contains($cursus)) {
            $this->cursuses[] = $cursus;
            $cursus->addEntranceGrade($this);
        }

        return $this;
    }

    public function removeCursus(Cursus $cursus): self
    {
        if ($this->cursuses->contains($cursus)) {
            $this->cursuses->removeElement($cursus);
            $cursus->removeEntranceGrade($this);
        }

        return $this;
    }
}

