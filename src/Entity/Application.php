<?php

namespace App\Entity;

use App\Repository\ApplicationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ApplicationRepository::class)
 */
class Application
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SchoolYear", inversedBy="applications")
     * @ORM\JoinColumn(name="school_year_id", referencedColumnName="id")
     */
    private $schoolYear;

    /**
     * @ORM\ManyToOne(targetEntity="EntranceGrade", inversedBy="applications")
     * @ORM\JoinColumn(name="entrance_grade_id", referencedColumnName="id")
     */
    private $entranceGrade;

    /**
     * @ORM\ManyToOne(targetEntity="Cursus", inversedBy="applications")
     * @ORM\JoinColumn(name="cursus_id", referencedColumnName="id")
     */
    private $cursus;

    /**
     * @ORM\ManyToOne(targetEntity="EntranceExamSession", inversedBy="applications")
     * @ORM\JoinColumn(name="entrance_exam_session_id", referencedColumnName="id")
     */
    private $entranceExamSession;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchoolYear(): ?SchoolYear
    {
        return $this->schoolYear;
    }

    public function setSchoolYear(?SchoolYear $schoolYear): self
    {
        $this->schoolYear = $schoolYear;

        return $this;
    }

    public function getEntranceGrade(): ?EntranceGrade
    {
        return $this->entranceGrade;
    }

    public function setEntranceGrade(?EntranceGrade $entranceGrade): self
    {
        $this->entranceGrade = $entranceGrade;

        return $this;
    }

    public function getCursus(): ?Cursus
    {
        return $this->cursus;
    }

    public function setCursus(?Cursus $cursus): self
    {
        $this->cursus = $cursus;

        return $this;
    }

    public function getEntranceExamSession(): ?EntranceExamSession
    {
        return $this->entranceExamSession;
    }

    public function setEntranceExamSession(?EntranceExamSession $entranceExamSession): self
    {
        $this->entranceExamSession = $entranceExamSession;

        return $this;
    }
}
