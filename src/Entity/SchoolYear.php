<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SchoolYearRepository;

/**
 * @ORM\Entity(repositoryClass=SchoolYearRepository::class)
 */
class SchoolYear
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /*
     * @ORM\ManyToMany(targetEntity="EntranceGrade", mappedBy="schoolYears", cascade={"all"})
     */
    private $entranceGrades;

    public function toArray()
    {
        return [
            'id'   => $this->getId(),
            'name' => $this->getName()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
