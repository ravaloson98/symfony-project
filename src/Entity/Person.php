<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 */
class Person
{
    const TYPE_STUDENT        = 10;
    const TYPE_PARENT_FATHER  = 20;
    const TYPE_PARENT_MOTHER  = 30;
    const TYPE_PARENT_TUTOR   = 40;

    const TYPE_MARITAL_SINGLE   = 10;
    const TYPE_MARITAL_MARRIED  = 20;
    const TYPE_MARITAL_DIVORCED = 30;
    const TYPE_MARITAL_WIDOW    = 40;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(groups={"parent"})
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="person", cascade={"PERSIST"})
     * @Assert\Valid()
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Applicant", inversedBy="relationships")
     * @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     * @Assert\Valid()
     */
    private $child;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"parent", "student"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"parent", "student"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"parent", "student"})
     */
    private $usageName;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $birthdate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $sex;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $birthCity;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="birthCountry_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"student"})
     */
    private $birthCountry;

    /**
     * @ORM\ManyToOne(targetEntity="Country")
     * @ORM\JoinColumn(name="nationality_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"student"})
     */
    private $nationality;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $maritalStatus;

    /**
     * @ORM\OneToOne(targetEntity="Document")
     * @ORM\JoinColumn(name="idCard_id", referencedColumnName="id")
     */
    private $idCard;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $secondaryAddress;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"student"})
     */
    private $mobilePhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Email(groups={"parent"})
     * @Assert\NotBlank(groups={"parent"})
     */
    private $primaryEmail; // if type student get from user

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Email
     */
    private $secondaryEmail;

    /**
     * @ORM\ManyToOne(targetEntity="JobType")
     * @ORM\JoinColumn(name="job_type_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"parent"})
     */
    private $jobType;

    /**
     * @ORM\ManyToOne(targetEntity="JobTitle")
     * @ORM\JoinColumn(name="job_title_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"parent"})
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotNull(groups={"parent"})
     */
    private $jobDetails;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="CompanyActivity")
     * @ORM\JoinColumn(name="company_activity_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"parent"})
     */
    private $companyActivity;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $companyPhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"parent"})
     */
    private $companyAddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsageName(): ?string
    {
        return $this->usageName;
    }

    public function setUsageName(?string $usageName): self
    {
        $this->usageName = $usageName;

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(?\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getSex(): ?bool
    {
        return $this->sex;
    }

    public function setSex(bool $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getBirthCity(): ?string
    {
        return $this->birthCity;
    }

    public function setBirthCity(string $birthCity): self
    {
        $this->birthCity = $birthCity;

        return $this;
    }

    public function getMaritalStatus(): ?int
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(int $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getSecondaryAddress(): ?string
    {
        return $this->secondaryAddress;
    }

    public function setSecondaryAddress(string $secondaryAddress): self
    {
        $this->secondaryAddress = $secondaryAddress;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMobilePhone(): ?string
    {
        return $this->mobilePhone;
    }

    public function setMobilePhone(string $mobilePhone): self
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    public function getPrimaryEmail(): ?string
    {
        return $this->primaryEmail;
    }

    public function setPrimaryEmail(string $primaryEmail): self
    {
        $this->primaryEmail = $primaryEmail;

        return $this;
    }

    public function getSecondaryEmail(): ?string
    {
        return $this->secondaryEmail;
    }

    public function setSecondaryEmail(string $secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getJobDetails(): ?string
    {
        return $this->jobDetails;
    }

    public function setJobDetails(string $jobDetails): self
    {
        $this->jobDetails = $jobDetails;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getCompanyPhone(): ?string
    {
        return $this->companyPhone;
    }

    public function setCompanyPhone(string $companyPhone): self
    {
        $this->companyPhone = $companyPhone;

        return $this;
    }

    public function getCompanyAddress(): ?string
    {
        return $this->companyAddress;
    }

    public function setCompanyAddress(string $companyAddress): self
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        // set (or unset) the owning side of the relation if necessary
        $newPerson = null === $user ? null : $this;
        if ($user->getPerson() !== $newPerson) {
            $user->setPerson($newPerson);
        }

        return $this;
    }

    public function getChild(): ?Applicant
    {
        return $this->child;
    }

    public function setChild(?Applicant $child): self
    {
        $this->child = $child;

        return $this;
    }

    public function getBirthCountry(): ?Country
    {
        return $this->birthCountry;
    }

    public function setBirthCountry(?Country $birthCountry): self
    {
        $this->birthCountry = $birthCountry;

        return $this;
    }

    public function getNationality(): ?Country
    {
        return $this->nationality;
    }

    public function setNationality(?Country $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getIdCard(): ?Document
    {
        return $this->idCard;
    }

    public function setIdCard(?Document $idCard): self
    {
        $this->idCard = $idCard;

        return $this;
    }

    public function getJobType(): ?JobType
    {
        return $this->jobType;
    }

    public function setJobType(?JobType $jobType): self
    {
        $this->jobType = $jobType;

        return $this;
    }

    public function getJobTitle(): ?JobTitle
    {
        return $this->jobTitle;
    }

    public function setJobTitle(?JobTitle $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getCompanyActivity(): ?CompanyActivity
    {
        return $this->companyActivity;
    }

    public function setCompanyActivity(?CompanyActivity $companyActivity): self
    {
        $this->companyActivity = $companyActivity;

        return $this;
    }
}
