<?php

namespace App\Entity;

use App\Repository\ApplicantRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ApplicantRepository::class)
 */
class Applicant
{
    const TYPE_BACCALAUREATE_A      = 10;
    const TYPE_BACCALAUREATE_A1     = 20;
    const TYPE_BACCALAUREATE_A2     = 30;
    const TYPE_BACCALAUREATE_L      = 40;
    const TYPE_BACCALAUREATE_C      = 50;
    const TYPE_BACCALAUREATE_D      = 60;
    const TYPE_BACCALAUREATE_S      = 70;
    const TYPE_BACCALAUREATE_G1     = 80;
    const TYPE_BACCALAUREATE_G2     = 90;
    const TYPE_BACCALAUREATE_OTHER  = 99;

    const TYPE_BACCALAUREATE_ACK_OK         = 10;
    const TYPE_BACCALAUREATE_ACK_GOOD       = 20;
    const TYPE_BACCALAUREATE_ACK_VERY_GOOD  = 30;

    const TYPE_LEAD_ORIGIN_AD       = 10;
    const TYPE_LEAD_ORIGIN_FRIEND   = 20;
    const TYPE_LEAD_ORIGIN_SCHOOL   = 30;

    const TYPE_BACCALAUREATE = 1;
    const TYPE_SAT = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="applicant")
     */
    private $user;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\PositiveOrZero()
     */
    private $familyMembersAtIscam;

    /**
     * @ORM\ManyToMany(targetEntity="Interest", inversedBy="applicants", cascade={"all"})
     * @ORM\JoinTable(name="applicants_interests")
     */
    private $interests;

    /**
     * @ORM\OneToMany(targetEntity="School", mappedBy="applicant")
     */
    private $schools;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"step-3"})
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"baccalaureate"})
     */
    private $baccalaureateType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"baccalaureate"})
     */
    private $baccalaureateAcknowledgement;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\NotBlank(groups={"baccalaureate"})
     */
    private $baccalaureateGrade;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"sat"})
     */
    private $satEbrwGrade;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(groups={"sat"})
     */
    private $satAlgebraGrade;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Range(
     *    min = 1950,
     *    max = 2010,
     *    minMessage = "L'année doit être au format AAAA (ex: 1999)",
     *    maxMessage = "L'année doit être au format AAAA (ex: 1999)",
     *    groups={"step-3"}
     * )
     */
    private $diplomaYear;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"step-3"})
     */
    private $diplomaPlace;

    /**
     * @ORM\OneToMany(targetEntity="Person", mappedBy="child", cascade={"PERSIST"})
     * @Assert\Valid()
     */
    private $relationships;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(min = 200,
     *    minMessage = "Le texte de votre objectif doit être composé d'au moins {{ limit }} caractères",
     *    allowEmptyString = false,
     *    groups={"step-3"}
     * )
     */
    private $goal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leadOrigin;

    /**
     * @ORM\OneToMany(targetEntity="Document", mappedBy="applicant")
     */
    private $documents;

    public function __construct()
    {
        $this->interests = new ArrayCollection();
        $this->relationships = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->birthDate = new \DateTime();
        $this->schools = new ArrayCollection();
    } // TO DO ensure there is at least a diploma, and a motivation letter

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFamilyMembersAtIscam(): ?int
    {
        return $this->familyMembersAtIscam;
    }

    public function setFamilyMembersAtIscam(int $familyMembersAtIscam): self
    {
        $this->familyMembersAtIscam = $familyMembersAtIscam;

        return $this;
    }

    public function getBaccalaureateType(): ?int
    {
        return $this->baccalaureateType;
    }

    public function setBaccalaureateType(?int $baccalaureateType): self
    {
        $this->baccalaureateType = $baccalaureateType;

        return $this;
    }

    public function getBaccalaureateAcknowledgement(): ?int
    {
        return $this->baccalaureateAcknowledgement;
    }

    public function setBaccalaureateAcknowledgement(?int $baccalaureateAcknowledgement): self
    {
        $this->baccalaureateAcknowledgement = $baccalaureateAcknowledgement;

        return $this;
    }

    public function getBaccalaureateGrade(): ?float
    {
        return $this->baccalaureateGrade;
    }

    public function setBaccalaureateGrade(?float $baccalaureateGrade): self
    {
        $this->baccalaureateGrade = $baccalaureateGrade;

        return $this;
    }

    public function getSatEbrwGrade(): ?int
    {
        return $this->satEbrwGrade;
    }

    public function setSatEbrwGrade(?int $satEbrwGrade): self
    {
        $this->satEbrwGrade = $satEbrwGrade;

        return $this;
    }

    public function getSatAlgebraGrade(): ?int
    {
        return $this->satAlgebraGrade;
    }

    public function setSatAlgebraGrade(?int $satAlgebraGrade): self
    {
        $this->satAlgebraGrade = $satAlgebraGrade;

        return $this;
    }

    public function getDiplomaYear(): ?\DateTimeInterface
    {
        return $this->diplomaYear;
    }

    public function setDiplomaYear(\DateTimeInterface $diplomaYear): self
    {
        $this->diplomaYear = $diplomaYear;

        return $this;
    }

    public function getDiplomaPlace(): ?string
    {
        return $this->diplomaPlace;
    }

    public function setDiplomaPlace(string $diplomaPlace): self
    {
        $this->diplomaPlace = $diplomaPlace;

        return $this;
    }

    public function getGoal(): ?string
    {
        return $this->goal;
    }

    public function setGoal(string $goal): self
    {
        $this->goal = $goal;

        return $this;
    }

    public function getLeadOrigin(): ?int
    {
        return $this->leadOrigin;
    }

    public function setLeadOrigin(int $leadOrigin): self
    {
        $this->leadOrigin = $leadOrigin;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        // set (or unset) the owning side of the relation if necessary
        $newApplicant = null === $user ? null : $this;
        if ($user->getApplicant() !== $newApplicant) {
            $user->setApplicant($newApplicant);
        }

        return $this;
    }

    /**
     * @return Collection|Interest[]
     */
    public function getInterests(): Collection
    {
        return $this->interests;
    }

    public function addInterest(Interest $interest): self
    {
        if (!$this->interests->contains($interest)) {
            $this->interests[] = $interest;
            $interest->addApplicant($this);
        }

        return $this;
    }

    public function removeInterest(Interest $interest): self
    {
        if ($this->interests->contains($interest)) {
            $this->interests->removeElement($interest);
            $interest->removeApplicant($this);
        }

        return $this;
    }

    public function hasRelationship(Person $relationship): bool
    {
        $relationships = $this->getRelationships();

        foreach ($relationships as $relationshipCursor) {
            if ($relationshipCursor->getId() === $relationship->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Collection|Person[]
     */
    public function getRelationships(): Collection
    {
        return $this->relationships;
    }

    public function addRelationship(Person $relationship): self
    {
        if (!$this->relationships->contains($relationship)) {
            $this->relationships[] = $relationship;
            $relationship->setChild($this);
        }

        return $this;
    }

    public function removeRelationship(Person $relationship): self
    {
        if ($this->relationships->contains($relationship)) {
            $this->relationships->removeElement($relationship);
            // set the owning side to null (unless already changed)
            if ($relationship->getChild() === $this) {
                $relationship->setChild(null);
            }
        }

        return $this;
    }

    public function getMainPerson()
    {
        foreach($this->getRelationships() as $person) {
            if ($person->getType(Person::TYPE_STUDENT)) {
                return $person;
            }
        }

        return null;
    }

    public function setMainPerson(Person $person)
    {
        if (!$this->getMainPerson()) {
            $person->setType(Person::TYPE_STUDENT);
            $this->addRelationship($person);

            return $this;
        }

        return false;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setApplicant($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getApplicant() === $this) {
                $document->setApplicant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|School[]
     */
    public function getSchools(): Collection
    {
        return $this->schools;
    }

    public function addSchool(School $school): self
    {
        if (!$this->schools->contains($school)) {
            $this->schools[] = $school;
            $school->setApplicant($this);
        }

        return $this;
    }

    public function removeSchool(School $school): self
    {
        if ($this->schools->contains($school)) {
            $this->schools->removeElement($school);
            // set the owning side to null (unless already changed)
            if ($school->getApplicant() === $this) {
                $school->setApplicant(null);
            }
        }

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }
}
