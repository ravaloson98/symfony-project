<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CursusRepository;


/**
 * @ORM\Entity(repositoryClass=CursusRepository::class)
 */
class Cursus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="EntranceGrade", inversedBy="cursuses", cascade={"all"})
     * @ORM\JoinTable(name="entrance_grades_cursuses")
     */
    private $entranceGrades;

    public function __construct()
    {
        $this->entranceGrades = new ArrayCollection();
    }

    public function toArray()
    {
        return [
            'id'   => $this->getId(),
            'name' => $this->getName()
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|EntranceGrade[]
     */
    public function getEntranceGrades(): Collection
    {
        return $this->entranceGrades;
    }

    public function addEntranceGrade(EntranceGrade $entranceGrade): self
    {
        if (!$this->entranceGrades->contains($entranceGrade)) {
            $this->entranceGrades[] = $entranceGrade;
        }

        return $this;
    }

    public function removeEntranceGrade(EntranceGrade $entranceGrade): self
    {
        if ($this->entranceGrades->contains($entranceGrade)) {
            $this->entranceGrades->removeElement($entranceGrade);
        }

        return $this;
    }
}

