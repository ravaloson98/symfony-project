<?php

namespace App\Repository;

use App\Entity\EntranceExamSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EntranceExamSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntranceExamSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntranceExamSession[]    findAll()
 * @method EntranceExamSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntranceExamSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntranceExamSession::class);
    }

    // /**
    //  * @return EntranceExamSession[] Returns an array of EntranceExamSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExamSession
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
