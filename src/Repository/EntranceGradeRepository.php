<?php

namespace App\Repository;

use App\Entity\EntranceGrade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EntranceGrade|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntranceGrade|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntranceGrade[]    findAll()
 * @method EntranceGrade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntranceGradeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntranceGrade::class);
    }

    // /**
    //  * @return EntranceGrade[] Returns an array of EntranceGrade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EntranceGrade
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
