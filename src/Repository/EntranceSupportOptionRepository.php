<?php

namespace App\Repository;

use App\Entity\EntranceSupportOption;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EntranceSupportOption|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntranceSupportOption|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntranceSupportOption[]    findAll()
 * @method EntranceSupportOption[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntranceSupportOptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntranceSupportOption::class);
    }

    // /**
    //  * @return EntranceSupportOption[] Returns an array of EntranceSupportOption objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EntranceSupportOption
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
