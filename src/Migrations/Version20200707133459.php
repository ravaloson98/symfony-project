<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200707133459 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE applicant (id INT AUTO_INCREMENT NOT NULL, family_members_at_iscam INT DEFAULT NULL, baccalaureate_type INT DEFAULT NULL, baccalaureate_acknowledgement INT DEFAULT NULL, baccalaureate_grade DOUBLE PRECISION DEFAULT NULL, sat_ebrw_grade INT DEFAULT NULL, sat_algebra_grade INT DEFAULT NULL, diploma_year DATE DEFAULT NULL, diploma_place VARCHAR(255) DEFAULT NULL, goal LONGTEXT DEFAULT NULL, lead_origin INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE applicants_interests (applicant_id INT NOT NULL, interest_id INT NOT NULL, INDEX IDX_667D0C7D97139001 (applicant_id), INDEX IDX_667D0C7D5A95FF89 (interest_id), PRIMARY KEY(applicant_id, interest_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE application (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_activity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, code INT NOT NULL, alpha2 VARCHAR(255) NOT NULL, alpha3 VARCHAR(255) NOT NULL, name_fr VARCHAR(255) NOT NULL, name_en VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, applicant_id INT DEFAULT NULL, type INT NOT NULL, number VARCHAR(255) DEFAULT NULL, delivery_date DATE DEFAULT NULL, delivery_place VARCHAR(255) DEFAULT NULL, expiry_date DATE DEFAULT NULL, INDEX IDX_D8698A7697139001 (applicant_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entrance_exam (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entrance_exam_session (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entrance_support_option (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE interest (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_title (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE job_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE person (id INT AUTO_INCREMENT NOT NULL, child_id INT DEFAULT NULL, nationality_id INT DEFAULT NULL, job_type_id INT DEFAULT NULL, job_title_id INT DEFAULT NULL, company_activity_id INT DEFAULT NULL, type INT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, usage_name VARCHAR(255) NOT NULL, birthdate DATE DEFAULT NULL, sex TINYINT(1) DEFAULT NULL, birth_city VARCHAR(255) DEFAULT NULL, marital_status INT DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, secondary_address VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, mobile_phone VARCHAR(255) DEFAULT NULL, primary_email VARCHAR(255) DEFAULT NULL, secondary_email VARCHAR(255) DEFAULT NULL, job_details VARCHAR(255) DEFAULT NULL, company VARCHAR(255) DEFAULT NULL, company_phone VARCHAR(255) DEFAULT NULL, company_address VARCHAR(255) DEFAULT NULL, birthCountry_id INT DEFAULT NULL, idCard_id INT DEFAULT NULL, INDEX IDX_34DCD176DD62C21B (child_id), INDEX IDX_34DCD1765DEBD613 (birthCountry_id), INDEX IDX_34DCD1761C9DA55 (nationality_id), UNIQUE INDEX UNIQ_34DCD1765A75F1D2 (idCard_id), INDEX IDX_34DCD1765FA33B08 (job_type_id), INDEX IDX_34DCD1766DD822C6 (job_title_id), INDEX IDX_34DCD1769811D80E (company_activity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE school (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, place VARCHAR(255) NOT NULL, grade VARCHAR(255) NOT NULL, year VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, applicant_id INT DEFAULT NULL, person_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, api_key VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D649C912ED9D (api_key), UNIQUE INDEX UNIQ_8D93D64997139001 (applicant_id), UNIQUE INDEX UNIQ_8D93D649217BBB47 (person_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE applicants_interests ADD CONSTRAINT FK_667D0C7D97139001 FOREIGN KEY (applicant_id) REFERENCES applicant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE applicants_interests ADD CONSTRAINT FK_667D0C7D5A95FF89 FOREIGN KEY (interest_id) REFERENCES interest (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE document ADD CONSTRAINT FK_D8698A7697139001 FOREIGN KEY (applicant_id) REFERENCES applicant (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD176DD62C21B FOREIGN KEY (child_id) REFERENCES applicant (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1765DEBD613 FOREIGN KEY (birthCountry_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1761C9DA55 FOREIGN KEY (nationality_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1765A75F1D2 FOREIGN KEY (idCard_id) REFERENCES document (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1765FA33B08 FOREIGN KEY (job_type_id) REFERENCES job_type (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1766DD822C6 FOREIGN KEY (job_title_id) REFERENCES job_title (id)');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1769811D80E FOREIGN KEY (company_activity_id) REFERENCES company_activity (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64997139001 FOREIGN KEY (applicant_id) REFERENCES applicant (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649217BBB47 FOREIGN KEY (person_id) REFERENCES person (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE applicants_interests DROP FOREIGN KEY FK_667D0C7D97139001');
        $this->addSql('ALTER TABLE document DROP FOREIGN KEY FK_D8698A7697139001');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD176DD62C21B');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64997139001');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1769811D80E');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1765DEBD613');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1761C9DA55');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1765A75F1D2');
        $this->addSql('ALTER TABLE applicants_interests DROP FOREIGN KEY FK_667D0C7D5A95FF89');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1766DD822C6');
        $this->addSql('ALTER TABLE person DROP FOREIGN KEY FK_34DCD1765FA33B08');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649217BBB47');
        $this->addSql('DROP TABLE applicant');
        $this->addSql('DROP TABLE applicants_interests');
        $this->addSql('DROP TABLE application');
        $this->addSql('DROP TABLE company_activity');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE entrance_exam');
        $this->addSql('DROP TABLE entrance_exam_session');
        $this->addSql('DROP TABLE entrance_support_option');
        $this->addSql('DROP TABLE interest');
        $this->addSql('DROP TABLE job_title');
        $this->addSql('DROP TABLE job_type');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE school');
        $this->addSql('DROP TABLE user');
    }
}
