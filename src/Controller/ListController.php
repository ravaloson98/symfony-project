<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\Country;
use App\Entity\Interest;
use App\Entity\JobType;
use App\Entity\JobTitle;
use App\Entity\CompanyActivity;
use App\Entity\SchoolYear;
use App\Entity\EntranceGrade;
use App\Entity\Cursus;

final class ListController extends AbstractController
{
    /**
     * @Route("/list/countries", name="list_countries", methods={"GET", "HEAD"})
     */
    public function listCountries() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $countries = $entityManager->getRepository(Country::class)->findAll();

        $countriesArray = [];

        foreach($countries as $country) {
            $countriesArray[] = $country->toArray();
        }

        return new JsonResponse($countriesArray);
    }

    /**
     * @Route("/list/interests", name="list_interests", methods={"GET", "HEAD"})
     */
    public function listInterests() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $interests = $entityManager->getRepository(Interest::class)->findAll();

        $interestsArray = [];

        foreach($interests as $interest) {
            $interestsArray[] = $interest->toArray();
        }

        return new JsonResponse($interestsArray);
    }

    /**
     * @Route("/list/job_types", name="list_job_types", methods={"GET", "HEAD"})
     */
    public function listJobTypes() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $jobTypes = $entityManager->getRepository(JobType::class)->findAll();

        $jobTypesArray = [];

        foreach($jobTypes as $jobType) {
            $jobTypesArray[] = $jobType->toArray();
        }

        return new JsonResponse($jobTypesArray);
    }

    /**
     * @Route("/list/job_titles", name="list_job_titles", methods={"GET", "HEAD"})
     */
    public function listJobTitles() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $jobTitles = $entityManager->getRepository(JobTitle::class)->findAll();

        $jobTitlesArray = [];

        foreach($jobTitles as $jobTitle) {
            $jobTitlesArray[] = $jobTitle->toArray();
        }

        return new JsonResponse($jobTitlesArray);
    }

    /**
     * @Route("/list/company_activities", name="list_company_activities", methods={"GET", "HEAD"})
     */
    public function listCompanyActivities() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $companyActivities = $entityManager->getRepository(CompanyActivity::class)->findAll();

        $companyActivitiesArray = [];

        foreach($companyActivities as $companyActivity) {
            $companyActivitiesArray[] = $companyActivity->toArray();
        }

        return new JsonResponse($companyActivitiesArray);
    }

    /**
     * @Route("/list/school_years", name="list_school_years", methods={"GET"})
     */
    public function listSchoolYears() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $schoolYears = $entityManager->getRepository(SchoolYear::class)->findAll();

        $schoolYearsArray = [];

        foreach($schoolYears as $schoolYear) {
            $schoolYearsArray[] = $schoolYear->toArray();
        }

        return new JsonResponse($schoolYearsArray);
    }

    /**
     * @Route("/list/entrance_grades", name="list_entrance_grades", methods={"GET"})
     */
    public function listEntranceGrades() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entranceGrades = $entityManager->getRepository(EntranceGrade::class)->findAll();

        $entranceGradesArray = [];

        foreach($entranceGrades as $entranceGrade) {
            $entranceGradesArray[] = $entranceGrade->toArray();
        }

        return new JsonResponse($entranceGradesArray);
    }

    /**
     * @Route("/list/cursuses", name="list_cursuses", methods={"GET"})
     */
    public function listCursuses() : JsonResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $cursuses = $entityManager->getRepository(Cursus::class)->findAll();

        $cursusesArray = [];

        foreach($cursuses as $cursus) {
            $cursusesArray[] = $cursus->toArray();
        }

        return new JsonResponse($cursusesArray);
    }
}
