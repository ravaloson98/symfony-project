<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

final class ApiStatusController
{
    public function root(): JsonResponse
    {
        return new JsonResponse(
            [[
                'status' => 'ok',
                'version' => 1
            ]],
            JsonResponse::HTTP_OK
        );
    }
}
