<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;

class SessionController
{
    public function getSession()
    {
        // get current session

        return new Response(
            json_encode([['status' => 'ok']]),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }

    public function postSession()
    {
        // create session (login)

        return new Response(
            json_encode([['status' => 'ok']]),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }

    public function deleteSession()
    {
        // delete session (logout)

        return new Response(
            json_encode([['status' => 'ok']]),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }
}
