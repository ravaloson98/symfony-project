<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController
{
    public function applicationCreate(Request $request)
    {

        return new Response(
            json_encode([['status' => 'ok']]),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
    }
}
