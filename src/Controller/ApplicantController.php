<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\Applicant;
use App\Entity\Person;
use App\Entity\School;
use App\Form\Type\StepOneType;
use App\Form\Type\StepTwoType;
use App\Form\Type\StepThreeType;
use App\Form\Type\SchoolType;

class ApplicantController extends AbstractController
{
    /**
     * @Route("/register/step-1", name="register_step1", methods={"POST"})
     */
    public function stepOne(Request $request, UserPasswordEncoderInterface $encoder) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $applicant = new Applicant();
        $form = $this->createForm(StepOneType::class, $applicant);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $applicant->getMainPerson()->getUser()->setPassword($encoder->encodePassword($applicant->getMainPerson()->getUser(), $applicant->getMainPerson()->getUser()->getPassword()));
            $applicant->setUser($applicant->getMainPerson()->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($applicant);

            foreach ($applicant->getInterests() as $interest) {
                $interest->addApplicant($applicant);
                $em->persist($interest);
            }

            $em->flush();

            $response = $applicant->getMainPerson()->getUser()->getApiKey();
        } else {
            $response = $this->convertFormToArray($form);

            return new JsonResponse(
                [ $response ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            [ $response ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/register/step-2", name="register_step2", methods={"POST"})
     */
    public function stepTwo(Request $request, Security $security) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $applicant = $security->getUser()->getApplicant();
        $person = new Person();
        $form = $this->createForm(StepTwoType::class, $person);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $applicant->addRelationship($person);
            $em->persist($applicant);
            $em->persist($person);
            $em->flush();

            $response = $person->getId();
        } else {
            $response = $this->convertFormToArray($form);

            return new JsonResponse(
                [ $response ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            [ $response ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/register/step-3", name="register_step3", methods={"POST"})
     */
    public function stepThree(Request $request, Security $security) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $applicant = $security->getUser()->getApplicant();
        $form = $this->createForm(StepThreeType::class, $applicant);
        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($applicant);
            $em->flush();

            $response = $applicant->getId();
        } else {
            $response = $this->convertFormToArray($form);

            return new JsonResponse(
                [ $response ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            [ $response ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/person/{id}", name="person_delete", methods={"DELETE"})
     */
    public function deletePerson(Request $request, Person $person, Security $security) : JsonResponse
    {
        $applicant = $security->getUser()->getApplicant();

        if (!$applicant->hasRelationship($person)) {
            return new JsonResponse(
                [],
                JsonResponse::HTTP_UNAUTHORIZED
            );
        }

        $em = $this->getDoctrine()->getManager();
        $applicant->removeRelationship($person);
        $em->persist($applicant);
        $em->remove($person);
        $em->flush();

        return new JsonResponse(
            [],
            JsonResponse::HTTP_OK
        );

    }

    /**
     * @Route("/applicant/school", name="school_create", methods={"POST"})
     */
    public function createSchool(Request $request, Security $security) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $applicant = $security->getUser()->getApplicant();
        $school = new School();
        $form = $this->createForm(SchoolType::class, $school);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $school->setApplicant($applicant);
            $em->persist($school);
            $em->flush();

            $response = $school->getId();
        } else {
            $response = $this->convertFormToArray($form);

            return new JsonResponse(
                [ $response ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            [ $response ],
            JsonResponse::HTTP_OK
        );
    }

    private function convertFormToArray(FormInterface $data)
    {
        $normalizer = new CamelCaseToSnakeCaseNameConverter();
        $propertyErrors = [];

        foreach ($data->getErrors() as $error) {
            $propertyPath = $error->getCause()->getPropertyPath();
            if ($propertyPath) {
                $propertyErrors[$normalizer->normalize($error->getCause()->getPropertyPath())] = $error->getMessage();
            }
        }

        foreach ($data->all() as $child) {
            if ($child instanceof FormInterface) {
                foreach ($this->convertFormToArray($child) as $field => $message) {
                    $propertyErrors[$field] = $message;
                }
            }
        }

        return $propertyErrors;
    }
}
